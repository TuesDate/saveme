package saveme.fol;

import java.util.HashMap;
import java.util.Set;


/**
 * @author Stamatis Pitsios
 * Modified by Anisha Inas Izdihar and Aviliani Pramestya
 * <p>
 * Every instance of this class represents a unifier theta.
 */
public class Unifier {
    private HashMap<Term, Term> theta;


    public Unifier() {
        theta = new HashMap<Term, Term>();
    }


    public void addSubstitution(Term key, Term subst) {
        theta.put(key, subst);
    }


    public boolean containsSubstitution(Term key) {
        return this.theta.containsKey(key);
    }


    public Term getSubstitution(Term key) {
        return this.theta.get(key);
    }


    public Set<Term> getKeys() {
        return this.theta.keySet();
    }


    @Override
    public String toString() {
        StringBuilder toReturn = new StringBuilder();

        for (Term term : theta.keySet()) {
            if (!toReturn.toString().equals("")) toReturn.append(" , ");
            toReturn.append(term.toString()).append("/").append(theta.get(term).toString());
        }

        return "{ " + toReturn + " } ";
    }


}