package saveme.fol;

/**
 * @author Stamatis Pitsios
 * Modified by Anisha Inas Izdihar and Aviliani Pramestya
 *
 * This class contains all the symbols that will be used as Literal names.
 */
public class LiteralSymbols {
    //Fire(x,y).
    public static String fire = "Fire";

    //Smoke(x,y).
    public static String smoke = "Smoke";

    //Villager(x,y).
    public static String villager = "Villager";

    //Help(x,y).
    public static String help = "Help";

    //OK(x,y).
    public static String ok = "OK";

    //FireFree(x,y) = not Fire(x,y).
    public static String firefree = "FireFree";

    //VillagerFree(x,y) = not Korban(x,y).
    public static String villagerfree = "VillagerFree";

    //Center_Square(x,y).
    public static String cs = "Center_Square";

    //Left_Square(x,y).
    public static String ls = "Left_Square";

    //Right_Square(x,y).
    public static String rs = "Right_Square";

    //Top_Square(x,y).
    public static String ts = "Top_Square";

    //Bottom_Square(x,y).
    public static String bs = "Bottom_Square";

    //Top_Left_Square(x,y).
    public static String tls = "Top_Left_Square";

    //Top_Right_Square(x,y).
    public static String trs = "Top_Right_Square";

    //Bottom_Left_Square(x,y).
    public static String bls = "Bottom_Left_Square";

    //Bottom_Right_Square.
    public static String brs = "Bottom_Right_Square";

    //InBounds(x) :  0<=x<map size.
    public static String inbounds = "InBounds";

    //OutOfBounds(x) :  x>map size or x<0.
    public static String outbounds = "OutOfBounds";

}