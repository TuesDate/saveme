package saveme.fol;

/**
 * @author Stamatis Pitsios
 * Modified by Anisha Inas Izdihar and Aviliani Pramestya
 * <p>
 * This class contains all the symbols that will be used as function names.
 */
public class FunctionSymbols {
    public static String add = "Add";
    public static String sub = "Sub";
}