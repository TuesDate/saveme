package saveme.fol;

/**
 * @author Stamatis Pitsios
 * Modified by Anisha Inas Izdihar and Aviliani Pramestya
 * <p>
 * Every instance of this class represents a constant.
 * A constant is an integer number that can be used to show positions of the agent.
 */
public class Constant extends Term {
    private int constant;


    public Constant() {
        constant = 0;
    }


    public Constant(int constant) {
        this.constant = constant;
    }

    public int getConstant() {
        return this.constant;
    }

    public void setConstant(int constant) {
        this.constant = constant;
    }

    @Override
    public boolean equals(Object ob) {
        if (ob.getClass() != this.getClass()) return false;

        Constant t = (Constant) ob;

        return t.constant == this.constant;
    }


    @Override
    public String toString() {
        return String.valueOf(this.constant);
    }


    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }
}