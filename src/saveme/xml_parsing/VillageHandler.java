package saveme.xml_parsing;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import saveme.rescue.Position;

/**
 * @author Stamatis Pitsios
 * Modified by Anisha Inas Izdihar and Aviliani Pramestya
 * <p>
 * This class is responsible for handling the xml parsing process.
 */
public class VillageHandler extends DefaultHandler {
    /**
     * The board of the game.
     */
    private String[][] board;

    /**
     * Indicates if we are parsing through a "Village" tag.
     */
    private boolean parsingVillage;

    /**
     * Indicates if we are parsing through a "BoardSize" tag.
     */
    private boolean parsingSize;

    /**
     * Indicates if we are parsing through a "AgentPosition" tag.
     */
    private boolean parsingAgentPos;

    /**
     * Indicates if we are parsing through a "row" tag.
     */
    private boolean parsingRow;

    /**
     * Indicates if we are parsing through a "col" tag.
     */
    private boolean parsingCol;

    /**
     * The row on the board of an object.
     */
    private int row;

    /**
     * The column on the board of an object.
     */
    private int col;

    /**
     * The size of the world.
     */
    private int size;

    /**
     * The agent's initial Position.
     */
    private Position pos;


    /**
     * Constructor.
     */
    public VillageHandler() {
        this.parsingAgentPos = false;
        this.parsingSize = false;
        this.parsingVillage = false;

        this.row = -1;
        this.col = -1;

        this.pos = new Position();
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        if (qName.equalsIgnoreCase("saveme")) {
            this.parsingVillage = true;
        } else if (qName.equalsIgnoreCase("boardsize")) {
            this.parsingSize = true;
        } else if (qName.equalsIgnoreCase("agentposition")) {
            this.parsingAgentPos = true;
        } else if (qName.equalsIgnoreCase("fireposition")) {
            // Nothing to do.
        } else if (qName.equalsIgnoreCase("villagerposition")) {
            // Nothing to do.
        } else if (qName.equalsIgnoreCase("waterposition")) {
            // Nothing to do.
        } else if (qName.equalsIgnoreCase("row")) {
            this.parsingRow = true;
        } else if (qName.equalsIgnoreCase("col")) {
            this.parsingCol = true;
        }
    }


    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        String s = new String(ch, start, length).trim();

        if (s.length() > 0) {
            if (this.parsingVillage && this.parsingRow) {
                this.row = Integer.parseInt(s);

                if (this.parsingAgentPos) {
                    this.pos.setRow(row);
                }
            } else if (this.parsingVillage && this.parsingCol) {
                this.col = Integer.parseInt(s);

                if (this.parsingAgentPos) {
                    this.pos.setColumn(col);
                }
            } else if (this.parsingVillage && this.parsingSize) {
                this.size = Integer.parseInt(s);
            }
        }
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("village")) {
            this.parsingVillage = false;
        } else if (qName.equalsIgnoreCase("boardsize")) {
            this.parsingSize = false;
            this.initializeBoard();
        } else if (qName.equalsIgnoreCase("agentposition")) {
            this.parsingAgentPos = false;
        } else if (qName.equalsIgnoreCase("fireposition")) {
            if (board[row][col].equals("")) board[row][col] += "F";
            else board[row][col] += (",F");
        } else if (qName.equalsIgnoreCase("waterposition")) {
            if (board[row][col].equals("")) board[row][col] += "W";
            else board[row][col] += (",W");
        } else if (qName.equalsIgnoreCase("villagerposition")) {
            if (board[row][col].equals("")) board[row][col] += "V";
            else board[row][col] += (",V");
        } else if (qName.equalsIgnoreCase("row")) {
            this.parsingRow = false;
        } else if (qName.equalsIgnoreCase("col")) {
            this.parsingCol = false;
        }
    }


    /**
     * Initializes the board.
     */
    private void initializeBoard() {
        this.board = new String[this.size][this.size];

        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                this.board[i][j] = "";
            }
        }
        System.out.println(this.row);
        System.out.println("selesai init");
    }


    /**
     * @return board The game Board.
     */
    public String[][] getBoard() {
        return this.board;
    }


    /**
     * @return pos The agent's initial position.
     */
    public Position getPosition() {
        return this.pos;
    }
}