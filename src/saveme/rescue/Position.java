package saveme.rescue;


/**
 * @author Stamatis Pitsios
 * modified by Anisha Inas Izdihar dan Aviliani Pramestya
 * Every object of this class represents a Position on the board
 */
public class Position {
    //the vertical position.
    private int row;

    //the horizontal position
    private int col;


    public Position() {
        this.row = -1;
        this.col = -1;
    }


    public Position(int r, int c) {
        row = r;
        col = c;
    }


    public void setRow(int r) {
        row = r;
    }


    public int getRow() {
        return row;
    }


    public void setColumn(int col) {
        this.col = col;
    }


    public int getColumn() {
        return col;
    }


    @Override
    public String toString() {
        return ("row=" + String.valueOf(row) + ",column=" + String.valueOf(col));
    }


    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) return false;

        Position p = (Position) obj;

        if (p.col == this.col && p.row == this.row) return true;

        return false;
    }


}