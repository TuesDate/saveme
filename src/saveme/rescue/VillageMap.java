package saveme.rescue;

import saveme.xml_parsing.VillageParser;

public class VillageMap {
    /**
     * The symbol that indicates that a square is OK.
     */
    private final String Ok = "OK";

    /**
     * The symbol that indicates that a square contains a villager.
     */
    private final String Villager = "V";

    /**
     * The symbol that indicates that a square contains a help.
     */
    private final String Help = "H";

    /**
     * The symbol that indicates that a square contains a smoke.
     */
    private final String Smoke = "S";

    /**
     * The symbol that indicates that a square contains the fire.
     */
    private final String Fire = "F";

    /**
     * The symbol that indicates that a square contains the water.
     */
    private final String Water = "W";

    /**
     * The symbol that indicates that a square has been visited before.
     */
    private final String Visited = "D";

    /**
     * The original map of the game.
     */
    private String[][] map;

    /**
     * The agent's initial position.
     */
    private Position position;

    /**
     * The size of the village.
     */
    private int size;


    /**
     * Constructor.
     *
     * @param fileName The file that contains information about the village.
     */
    public VillageMap(String fileName) {
        VillageParser parser = new VillageParser(fileName);
        Object[] contents = parser.parse();

        this.map = (String[][]) contents[0];
        this.position = (Position) contents[1];
        this.size = map.length;

        this.AddRest();
        this.AddOk();

        printMap();
    }


    /**
     * @return size The size of the current village
     */
    public int getSize() {
        return this.size;
    }


    /**
     * @return position The agent's initial position.
     */
    public Position getInitialPosition() {
        return this.position;
    }

    /**
     * Returns the contents of the specified position of the map.
     *
     * @param row The row of the map.
     * @param col The column of the map.
     * @return map[row][col] The contents of the map specific cell.
     */
    public String GetData(int row, int col) {
        return map[row][col];
    }

    /**
     * Removes help from the map. This method is called after the agent found villager.
     */
    public void RemoveHelp(int row, int col) {
        if (map[row][col].contains(this.Smoke) || map[row][col].contains(this.Fire) || map[row][col].contains(this.Villager)) {
            map[row][col] = map[row][col].replace("," + this.Help, "");
            map[row][col] = map[row][col].replace(this.Help + ",", "");
            map[row][col] = map[row][col].replace(this.Help, "");
        } else {
            map[row][col] = map[row][col].replace("," + this.Help, "," + this.Ok);
            map[row][col] = map[row][col].replace(this.Help + ",", this.Ok + ",");
            map[row][col] = map[row][col].replace(this.Help, this.Ok);
        }
    }

    /**
     * Removes villager from the map. This method is called after the agent found villager.
     */
    public void RemoveVillager(int row, int col) {
        if (map[row][col].contains(this.Smoke) || map[row][col].contains(this.Help)) {
            map[row][col] = map[row][col].replace("," + this.Villager, "");
            map[row][col] = map[row][col].replace(this.Villager + ",", "");
            map[row][col] = map[row][col].replace(this.Villager, "");
        } else {
            map[row][col] = map[row][col].replace("," + this.Villager, "," + this.Ok);
            map[row][col] = map[row][col].replace(this.Villager + ",", this.Ok + ",");
            map[row][col] = map[row][col].replace(this.Villager, this.Ok);
        }
    }

    /**
     * Removes smoke from the map. This method is called after the agent put out fire.
     */
    public void RemoveSmoke(int row, int col) {
        if (map[row][col].contains(this.Help) || map[row][col].contains(this.Fire) || map[row][col].contains(this.Villager)) {
            map[row][col] = map[row][col].replace("," + this.Smoke, "");
            map[row][col] = map[row][col].replace(this.Smoke + ",", "");
            map[row][col] = map[row][col].replace(this.Smoke, "");
        } else {
            map[row][col] = map[row][col].replace("," + this.Smoke, "," + this.Ok);
            map[row][col] = map[row][col].replace(this.Smoke + ",", this.Ok + ",");
            map[row][col] = map[row][col].replace(this.Smoke, this.Ok);
        }
    }

    /**
     * Removes fire from the map. This method is called after the agent put out fire.
     */
    public void RemoveFire(int row, int col) {
        if (map[row][col].contains(this.Help) || map[row][col].contains(this.Smoke)) {
            map[row][col] = map[row][col].replace("," + this.Fire, "");
            map[row][col] = map[row][col].replace(this.Fire + ",", "");
            map[row][col] = map[row][col].replace(this.Fire, "");
        } else {
            map[row][col] = map[row][col].replace("," + this.Fire, "," + this.Ok);
            map[row][col] = map[row][col].replace(this.Fire + ",", this.Ok + ",");
            map[row][col] = map[row][col].replace(this.Fire, this.Ok);
        }
    }

    /**
     * Removes water from the map. This method is called after the agent found water source
     */
    public void removeWater(int row, int col) {
        if (map[row][col].contains(this.Help) || map[row][col].contains(this.Smoke)) {
            map[row][col] = map[row][col].replace("," + this.Water, "");
            map[row][col] = map[row][col].replace(this.Water + ",", "");
            map[row][col] = map[row][col].replace(this.Water, "");
        } else {
            map[row][col] = map[row][col].replace("," + this.Water, "," + this.Ok);
            map[row][col] = map[row][col].replace(this.Water + ",", this.Ok + ",");
            map[row][col] = map[row][col].replace(this.Water, this.Ok);
        }
    }


    /**
     * Adds the Smoke and Help tags on the map.
     */
    private void AddRest() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (map[i][j].contains(Fire)) {
                    //we must place the Smoke tag up , down , right and left of the fire.

                    //Up!
                    int row = i - 1;
                    if (row >= 0) {
                        if (map[row][j].equals("")) map[row][j] += Smoke;
                        else if (!map[row][j].contains(Smoke)) map[row][j] += ("," + Smoke);
                    }


                    //Down!
                    row = i + 1;
                    if (row < size) {
                        if (map[row][j].equals("")) map[row][j] += Smoke;
                        else if (!map[row][j].contains(Smoke)) map[row][j] += ("," + Smoke);
                    }


                    //Right!
                    int col = j + 1;
                    if (col < size) {
                        if (map[i][col].equals("")) map[i][col] += Smoke;
                        else if (!map[i][col].contains(Smoke)) map[i][col] += ("," + Smoke);
                    }


                    //Left!
                    col = j - 1;
                    if (col >= 0) {
                        if (map[i][col].equals("")) map[i][col] += Smoke;
                        else if (!map[i][col].contains(Smoke)) map[i][col] += ("," + Smoke);
                    }
                }


                if (map[i][j].contains(Villager)) {
                    //we must place the Help tag up , down , right and left of the Villager.

                    //Up!
                    int row = i - 1;
                    if (row >= 0) {
                        if (map[row][j].equals("")) map[row][j] += Help;
                        else if (!map[row][j].contains(Help)) map[row][j] += ("," + Help);
                    }

                    //Up 2!
                    row = i - 2;
                    if (row >= 0) {
                        if (map[row][j].equals("")) map[row][j] += Help;
                        else if (!map[row][j].contains(Help)) map[row][j] += ("," + Help);
                    }

                    //Down!
                    row = i + 1;
                    if (row < size) {
                        if (map[row][j].equals("")) map[row][j] += Help;
                        else if (!map[row][j].contains(Help)) map[row][j] += ("," + Help);
                    }

                    //Down 2!
                    row = i + 2;
                    if (row < size) {
                        if (map[row][j].equals("")) map[row][j] += Help;
                        else if (!map[row][j].contains(Help)) map[row][j] += ("," + Help);
                    }

                    //Right!
                    int col = j + 1;
                    if (col < size) {
                        if (map[i][col].equals("")) map[i][col] += Help;
                        else if (!map[i][col].contains(Help)) map[i][col] += ("," + Help);
                    }

                    //Right 2!
                    col = j + 2;
                    if (col < size) {
                        if (map[i][col].equals("")) map[i][col] += Help;
                        else if (!map[i][col].contains(Help)) map[i][col] += ("," + Help);
                    }

                    //Left!
                    col = j - 1;
                    if (col >= 0) {
                        if (map[i][col].equals("")) map[i][col] += Help;
                        else if (!map[i][col].contains(Help)) map[i][col] += ("," + Help);
                    }

                    //Left 2!
                    col = j - 2;
                    if (col >= 0) {
                        if (map[i][col].equals("")) map[i][col] += Help;
                        else if (!map[i][col].contains(Help)) map[i][col] += ("," + Help);
                    }
                }
            }
        }

    }


    /**
     * Add the OK tag to the empty positions.
     */
    private void AddOk() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (map[i][j].equals("")) {
                    map[i][j] = Ok;
                }
            }
        }
    }

    /**
     * Prints the board that the agent has explored.
     */
    public void printMap() {
        for (int i = 0; i < map.length; i++) {
            System.out.print("\t" + String.valueOf(i));
        }

        System.out.println();

        for (int i = 0; i < map.length; i++) {
            System.out.print("\t*");
        }

        System.out.println();

        for (int row = 0; row < map.length; row++) {
            System.out.print("     " + String.valueOf(row) + "*");

            for (int col = 0; col < map.length; col++) {
                System.out.print("\t" + map[row][col]); // print each of the cells
            }

            System.out.print(" *");

            System.out.println("\n");
        }

        for (int i = 0; i < map.length; i++) {
            System.out.print("\t*");
        }

        System.out.println("\n\n\n\n\n");
    }

}
