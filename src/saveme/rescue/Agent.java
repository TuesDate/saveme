package saveme.rescue;

import saveme.fol.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Agent {
    // The number of times that we tolerate the agent to go back to a visited square;
    private final int backMoves = 1;
    // The current position of the agent.
    private Position current_position;
    // The agent's previous position.
    private Position previous_position;
    // The world that the agent has discovered.
    private String[][] world_explored;
    // The number of times the agent went to a visited square.
    private int backTimes;

    // The original game map.
    private VillageMap map;

    // The agent's knowledgeBase.
    private KnowledgeBase KB;

    /*
     * The following variables will help us to find the villager.
     */


    // energy count of agent
    private int energy;

    // counter of villager saved by agent
    private int villagerNotFound;

    // -1 if agent is not focused on looking for help
    // 0 if agent is focused on looking for help
    // 1 if agent has found 2 consequent help
    private int isLookingForHelp;

    // 0 if agent's next move is not risky
    // 1 if agent's next move is risky
    private int isRiskyToMove;

    // Buffered writer to write facts and output
    private BufferedWriter writer;

    // if true, agent can put out fire
    private boolean hasWater;

    //constructor
    public Agent(String fileName) {
        this.map = new VillageMap(fileName);
        this.current_position = map.getInitialPosition();
        this.previous_position = new Position();
        this.InitializeMap(map.getSize());
        this.KB = new KnowledgeBase();
        this.backTimes = 0;
        this.AddMapFacts();
        this.AddRules();
        this.energy = 100;
        this.villagerNotFound = 2;
        this.hasWater = false;
        this.isLookingForHelp = -1;
        this.isRiskyToMove = 0;

        try {
            this.writer = new BufferedWriter(new FileWriter("out.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * When this class is initiated, agent starts its journey to find the villager
     * Agent will check the current square they are in and add the facts regarding the square to KB
     * Agent will ask the KB which square he should go next
     * Every step the agent took will be written to output.txt
     */
    public void findVillager() {

        world_explored[this.current_position.getRow()][this.current_position.getColumn()] = map.GetData(this.current_position.getRow(), this.current_position.getColumn()) + ",A";
        this.printMap();
        this.AddFacts();
        if (GameOver()) return;

        while (true) {

            //make the safest and most effective move.
            Position toDo = this.MakeMove();

            //update the current map.
            this.UpdateWorld(toDo);

            //print the board.
            this.printMap();

            //adds new facts to the the knowledge base.
            this.AddFacts();

            //if the agent has found all villagers or ran out of energy, end the game an display journey in GUI
            if (this.GameOver()) {
                GUI gui = new GUI();
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
        }

    }


    //----------------------------------------SETTER GETTER----------------------------------------//

    private int getEnergy() {
        return energy;
    }

    private void setEnergy(int energy) {
        this.energy = energy;
    }

    private int getVillagerNotFound() {
        return villagerNotFound;
    }

    private int isLookingForHelp() {
        return isLookingForHelp;
    }

    private void setLookingForHelp(int lookingForHelp) {
        isLookingForHelp = lookingForHelp;
    }

    private void setIsRiskyToMove(int isRiskyToMove) {
        this.isRiskyToMove = isRiskyToMove;
    }

    /**
     * Initializes the map known by agent.
     *
     * @param size of map
     */
    private void InitializeMap(int size) {

        world_explored = new String[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                world_explored[i][j] = "-";
            }
        }

    }

    /**
     * prints the map that the agent has explored.
     * write the map to output.txt. the file will be used to display them in GUI
     */
    private void printMap() {

        System.out.println("villager not found: " + getVillagerNotFound());
        for (int i = 0; i < world_explored.length; i++) {
            System.out.print("\t" + String.valueOf(i));
        }

        System.out.println();

        for (int i = 0; i < world_explored.length; i++) {
            System.out.print("\t*");
        }

        System.out.println("\n");

        try {
            for (int row = 0; row < world_explored.length; row++) {
                System.out.print(String.valueOf(row) + "*");

                for (int col = 0; col < world_explored.length; col++) {
                    System.out.print("\t\t" + world_explored[row][col]); // print each of the cells
                    writer.write(world_explored[row][col]);
                    writer.newLine();
                }

                System.out.print("\t\t*" + String.valueOf(row));

                System.out.println("\n");
            }

            for (int i = 0; i < world_explored.length; i++) {
                System.out.print("\t\t*");
            }

            System.out.println();

            for (int i = 0; i < world_explored.length; i++) {
                System.out.print("\t\t" + String.valueOf(i));
            }

            System.out.println("\n\n\n\n\n");
            writer.write("xxx");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * checks if the game has ended by several reasons
     */
    private boolean GameOver() {

        // if current position contains fire and the agent's remaining energy is 10 and the agent has not got water, game over
        if (world_explored[current_position.getRow()][current_position.getColumn()].contains("F")
                && getEnergy() == 10 && !this.hasWater) {
            System.out.println("Agent stepped into Fire and doesn't have energy left. GAME IS OVER!");
            return true;
        }

        /*
         * if  current position contains fire and the agent has not got water then add facts to KB fire(row, col) villagerfree(row, col)
         * firefree(row+2, col) firefree(row-2, col) firefree(row, col+2) firefree(row, col-2)
         * firefree(row+1, col+1) firefree(row+1, col-1) firefree(row-1, col+1) firefree(row-1, col-1)
         */
        else if (world_explored[current_position.getRow()][current_position.getColumn()].contains("F")
                && !this.hasWater) {

            addFireFacts();
            setEnergy(getEnergy() - 10);
            System.out.println("Ouch! Agent stepped into fire! Agent energy decreased :(. Energy left: " + getEnergy());
            return false;

        }

        //if current position contains villager
        else if (world_explored[current_position.getRow()][current_position.getColumn()].contains("V")) {

            this.KB.addFact(new HornLiteral(LiteralSymbols.villager, true
                    , new Constant(current_position.getRow()), new Constant(current_position.getColumn())));
            this.KB.addFact(new HornLiteral(LiteralSymbols.firefree, true
                    , new Constant(current_position.getRow()), new Constant(current_position.getColumn())));

            System.out.println("Agent found a villager. Congratulations!!!");
            this.villagerNotFound = getVillagerNotFound() - 1;
            System.out.println("Agent still needs to find: " + getVillagerNotFound() + " villager");

            // stop rescue if all villagers has been found
            if (getVillagerNotFound() == 0) {
                System.out.println("YOU DID IT! You saved all villagers! GAME IS OVER!");
                return true;
            }

            return false;
        }

        System.out.println("Hmmm, the villager isn't here...");

        return false;
    }

    /**
     * add to KB the fact that there is fire in current position
     */
    private void addFireFacts() {
        KB.addFact(new HornLiteral(LiteralSymbols.fire, true, new Constant(current_position.getRow()), new Constant(current_position.getColumn())));
        KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(current_position.getRow()), new Constant(current_position.getColumn())));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow() + 2), new Constant(current_position.getColumn())));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow() - 2), new Constant(current_position.getColumn())));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow()), new Constant(current_position.getColumn() + 2)));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow()), new Constant(current_position.getColumn() - 2)));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow() + 1), new Constant(current_position.getColumn() + 1)));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow() + 1), new Constant(current_position.getColumn() - 1)));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow() - 1), new Constant(current_position.getColumn() + 1)));
        KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(current_position.getRow() - 1), new Constant(current_position.getColumn() - 1)));
    }

    /**
     * This method will update agent's personal knowledge about the village
     * Agent will check the condition of the current square they are in,
     * if the agent has heard help plea from villager,
     * they will try their best to reach the villager first
     *
     * @param nextPosition the next position the agent will go to
     */
    private void UpdateWorld(Position nextPosition) {

        String next = world_explored[nextPosition.getRow()][nextPosition.getColumn()];

        if (next.equals("-"))
            world_explored[nextPosition.getRow()][nextPosition.getColumn()] = map.GetData(nextPosition.getRow(), nextPosition.getColumn());

        printCurrentPosition(nextPosition);

        // get information about current position
        int currentRow = this.current_position.getRow();
        int currentCol = this.current_position.getColumn();
        String current = world_explored[currentRow][currentCol];

        // remove agent from current position
        world_explored[currentRow][currentCol] = current.substring(0, current.length() - 2);

        // update the map accoring to the information obtained in current position
        if (current.contains("W")) removeWater(currentRow, currentCol);
        if (current.contains("V")) removeVillager(currentRow, currentCol);
        if (current.contains("F") && this.hasWater) removeFire(currentRow, currentCol);
        if (!current.contains("V")) setNoVillager(currentRow, currentCol);
        if (!current.contains("D")) world_explored[currentRow][currentCol] += ",D";

        // if agent is in risky position where they doesn't have water but
        // they can go to squares that might have villagers in it,
        // just leave them and come back later
        if (isRiskyToMove == 1) {
            setLookingForHelp(-1);
        }

        // if agent has heard cry of help, focus on finding the villagers first
        if (this.isLookingForHelp() >= 0) {

            // if in current position does not contain help or villager, go back to previous position
            if (!current.contains("H") && !current.contains("V")) {

                if (next.equals("-")) world_explored[nextPosition.getRow()][nextPosition.getColumn()] = "-";

                this.current_position.setRow(previous_position.getRow());
                this.current_position.setColumn(previous_position.getColumn());
                this.previous_position.setRow(currentRow);
                this.previous_position.setColumn(currentCol);

                if (isLookingForHelp == 1) setLookingForHelp(0);
                if (isLookingForHelp == 0) setLookingForHelp(0);

            }

            // if in current square agent found another help, then continue investigate the area around
            else {

                this.previous_position.setRow(this.current_position.getRow());
                this.previous_position.setColumn(this.current_position.getColumn());
                this.current_position.setRow(nextPosition.getRow());
                this.current_position.setColumn(nextPosition.getColumn());

                if (isLookingForHelp == 0) setLookingForHelp(0);
                else if (isLookingForHelp == 1) setLookingForHelp(-1);

            }

        }

        // if in current square agent found help, then start focusing on finding the villagers
        else {

            // do not focus on finding villager if the area around is too risky
            if (world_explored[current_position.getRow()][current_position.getColumn()].contains("H") && isRiskyToMove != 1)
                setLookingForHelp(0);

            this.previous_position.setRow(this.current_position.getRow());
            this.previous_position.setColumn(this.current_position.getColumn());
            this.current_position.setRow(nextPosition.getRow());
            this.current_position.setColumn(nextPosition.getColumn());

        }

        // move agent to next position
        world_explored[current_position.getRow()][current_position.getColumn()] += ",A";

    }

    /**
     * This method will print previous, current, and next position to be visited
     *
     * @param nextPosition the next position the agent will go to
     */
    private void printCurrentPosition(Position nextPosition) {

        String prevMove = "-";
        String currentMove = "-";
        String nextMove = "-";
        if (isValidMove(previous_position)) {
            prevMove = world_explored[previous_position.getRow()][previous_position.getColumn()];
            System.out.println("prev: " + previous_position.getRow() + ":" + previous_position.getColumn() + " " + prevMove);
        }
        if (isValidMove(current_position)) {
            currentMove = world_explored[current_position.getRow()][current_position.getColumn()];
            System.out.println("current: " + current_position.getRow() + ":" + current_position.getColumn() + " " + currentMove);
        }
        if (isValidMove(nextPosition)) {
            nextMove = world_explored[nextPosition.getRow()][nextPosition.getColumn()];
            System.out.println("todo: " + nextPosition.getRow() + ":" + nextPosition.getColumn() + " " + nextMove);
        }
    }

    /**
     * informs KB there is no villager in position at this row and column
     *
     * @param row
     * @param column
     */
    private void setNoVillager(int row, int column) {
        KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(column)));
    }

    /**
     * when agent found a villager, villager will be removed from map, along with call for help around the area
     *
     * @param row
     * @param col
     */
    private void removeVillager(int row, int col) {
        String current = world_explored[row][col];

        if (current.contains("S") || current.contains("H")) {
            world_explored[row][col] = world_explored[row][col].replace(",V", "");
            world_explored[row][col] = world_explored[row][col].replace("V,", "");
            world_explored[row][col] = world_explored[row][col].replace("V", "");
        } else {
            world_explored[row][col] = world_explored[row][col].replace(",V", ",OK");
            world_explored[row][col] = world_explored[row][col].replace("V,", "OK,");
            world_explored[row][col] = world_explored[row][col].replace("V", "OK");
        }

        map.RemoveVillager(row, col);

        removeHelp(row + 1, col);
        removeHelp(row + 2, col);
        removeHelp(row - 1, col);
        removeHelp(row - 2, col);
        removeHelp(row, col + 1);
        removeHelp(row, col + 2);
        removeHelp(row, col - 1);
        removeHelp(row, col - 2);

        setLookingForHelp(-1);

    }

    private void removeHelp(int row, int col) {
        if (row > 5 || row < 0 || col > 5 || col < 0) return;

        String square = world_explored[row][col];

        if (square.contains("S") || square.contains("F") || square.contains("V")) {
            world_explored[row][col] = world_explored[row][col].replace(",H", "");
            world_explored[row][col] = world_explored[row][col].replace("H,", "");
            world_explored[row][col] = world_explored[row][col].replace("H", "");

        } else {
            world_explored[row][col] = world_explored[row][col].replace(",H", ",OK");
            world_explored[row][col] = world_explored[row][col].replace("H,", "OK,");
            world_explored[row][col] = world_explored[row][col].replace("H", "OK");
        }

        map.RemoveHelp(row, col);

    }

    /**
     * when agent found a water source, water will be removed from map.
     * add to KB mot to worry about fire anymore
     *
     * @param row
     * @param col
     */
    private void removeWater(int row, int col) {
        String square = world_explored[row][col];
        if (square.contains("S") || square.contains("H")) {
            world_explored[row][col] = world_explored[row][col].replace(",W", "");
            world_explored[row][col] = world_explored[row][col].replace("W,", "");
            world_explored[row][col] = world_explored[row][col].replace("W", "");
        } else {
            world_explored[row][col] = world_explored[row][col].replace(",W", ",OK");
            world_explored[row][col] = world_explored[row][col].replace("W,", "OK,");
            world_explored[row][col] = world_explored[row][col].replace("W", "OK");
        }

        this.hasWater = true;
        map.removeWater(row, col);
        KB.FireWasPutOut();
        this.map.printMap();

    }

    /**
     * when agent found fire and agent has found water,
     * villager will be able to put out fire and remove them from map, along with smokes around the area
     *
     * @param row
     * @param col
     */
    private void removeFire(int row, int col) {
        String square = world_explored[row][col];

        if (square.contains("S") || square.contains("H")) {
            world_explored[row][col] = world_explored[row][col].replace(",F", "");
            world_explored[row][col] = world_explored[row][col].replace("F,", "");
            world_explored[row][col] = world_explored[row][col].replace("F", "");
        } else {
            world_explored[row][col] = world_explored[row][col].replace(",F", ",OK");
            world_explored[row][col] = world_explored[row][col].replace("F,", "OK,");
            world_explored[row][col] = world_explored[row][col].replace("F", "OK");
        }

        this.hasWater = true;
        map.RemoveFire(row, col);

        removeSmoke(row - 1, col);
        removeSmoke(row + 1, col);
        removeSmoke(row, col + 1);
        removeSmoke(row, col - 1);

        KB.FireWasPutOut();
        this.map.printMap();
    }

    private void removeSmoke(int row, int col) {
        if (row > 5 || row < 0 || col > 5 || col < 0) return;

        String square = world_explored[row][col];

        if (square.contains("H") || square.contains("F") || square.contains("V")) {
            world_explored[row][col] = world_explored[row][col].replace(",S", "");
            world_explored[row][col] = world_explored[row][col].replace("S,", "");
            world_explored[row][col] = world_explored[row][col].replace("S", "");
        } else if (!square.contains("V")) {
            world_explored[row][col] = world_explored[row][col].replace(",S", ",OK");
            world_explored[row][col] = world_explored[row][col].replace("S,", "OK,");
            world_explored[row][col] = world_explored[row][col].replace("S", "OK");
        }

        map.RemoveSmoke(row, col);
    }


    /**
     * update KB according to the information found in current position
     */
    private void AddFacts() {
        int row = this.current_position.getRow();
        int col = this.current_position.getColumn();
        int size = this.world_explored.length;


        if (this.world_explored[row][col].contains("OK")) {
            //add the fact : OK(row , col).
            KB.addFact(new HornLiteral(LiteralSymbols.ok, true, new Constant(row), new Constant(col)));

            //add the facts : not Fire(row,col) , not Fire(row+1,col) , not Fire(row-1,col) , not Fire(row,col+1) , not Fire(row,col-1).
            KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(col)));

            int temp_row = row + 1;
            if (temp_row < size)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(temp_row), new Constant(col)));

            temp_row = row - 1;
            if (temp_row >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(temp_row), new Constant(col)));

            int temp_col = col + 1;
            if (temp_col < size)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(temp_col)));

            temp_col = col - 1;
            if (temp_col >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(temp_col)));


            //now add the facts : not Villager(row,col) ,  not Villager(row+1,col) , not Villager(row-1,col) , not Villager(row,col+1) , not Villager(row,col-1).
            KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(col)));

            temp_row = row + 1;
            if (temp_row < size)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(temp_row), new Constant(col)));

            temp_row = row - 1;
            if (temp_row >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(temp_row), new Constant(col)));

            temp_col = col + 1;
            if (temp_col < size)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(temp_col)));

            temp_col = col - 1;
            if (temp_col >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(temp_col)));

        } else if (this.world_explored[row][col].contains("S") && this.world_explored[row][col].contains("H")) {
            //add the facts : Smoke(row , col) , help(row,col).
            KB.addFact(new HornLiteral(LiteralSymbols.smoke, true, new Constant(row), new Constant(col)));
            KB.addFact(new HornLiteral(LiteralSymbols.help, true, new Constant(row), new Constant(col)));

            //add the facts : not Fire(row,col) , not Villager(row,col)
//            KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(col)));
//            KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(col)));

        } else if (this.world_explored[row][col].contains("S")) {
            //add the fact : Smoke(row , col).
            KB.addFact(new HornLiteral(LiteralSymbols.smoke, true, new Constant(row), new Constant(col)));

            //now add the facts : not Fire(row,col) , not Villager(row,col) , not Villager(row+1,col) , not Villager(row-1,col) , not Villager(row,col+1) , not Villager(row,col-1).
//            KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(col)));
//            KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(col)));

            int temp_row = row + 1;
            if (temp_row < size)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(temp_row), new Constant(col)));

            temp_row = row - 1;
            if (temp_row >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(temp_row), new Constant(col)));

            int temp_col = col + 1;
            if (temp_col < size)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(temp_col)));

            temp_col = col - 1;
            if (temp_col >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(temp_col)));
        } else if (this.world_explored[row][col].contains("H")) {
            //add the fact : help(row , col).
            KB.addFact(new HornLiteral(LiteralSymbols.help, true, new Constant(row), new Constant(col)));

            //add the facts : not Fire(row,col) , not Villager(row,col) , not Fire(row+1,col) , not Fire(row-1,col) , not Fire(row,col+1) , not Fire(row,col-1).
//            KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(col)));
//            KB.addFact(new HornLiteral(LiteralSymbols.villagerfree, true, new Constant(row), new Constant(col)));

            int temp_row = row + 1;
            if (temp_row < size)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(temp_row), new Constant(col)));

            temp_row = row - 1;
            if (temp_row >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(temp_row), new Constant(col)));

            int temp_col = col + 1;
            if (temp_col < size)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(temp_col)));

            temp_col = col - 1;
            if (temp_col >= 0)
                KB.addFact(new HornLiteral(LiteralSymbols.firefree, true, new Constant(row), new Constant(temp_col)));
        }

        try {
            BufferedWriter factsWriter = new BufferedWriter(new FileWriter("facts.txt"));
            factsWriter.write(this.KB.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * returns all the available moves that the agent can perform.
     */
    private ArrayList<Position> getChildren() {
        /*
         * First we generate all the moves that the agent can do.
         * These are : one block right , one block left , one block up and one block down
         */
        Position right = new Position(current_position.getRow(), current_position.getColumn() + 1);
        Position left = new Position(current_position.getRow(), current_position.getColumn() - 1);
        Position up = new Position(current_position.getRow() - 1, current_position.getColumn());
        Position down = new Position(current_position.getRow() + 1, current_position.getColumn());

        ArrayList<Position> moves = new ArrayList<Position>();

        if (isValidMove(right)) moves.add(right);
        if (isValidMove(left)) moves.add(left);
        if (isValidMove(up)) moves.add(up);
        if (isValidMove(down)) moves.add(down);

        return moves;
    }


    /**
     * checks if a move that the agent can do is valid.
     */
    private boolean isValidMove(Position pos) {
        int row = pos.getRow();
        int column = pos.getColumn();

        if (row > world_explored.length - 1 || row < 0) return false;

        if (column > world_explored.length - 1 || column < 0) return false;

        return true;
    }


    /**
     * makes the most suitable move.
     */
    private Position MakeMove() {
        setIsRiskyToMove(0);

        Random rand = new Random();

        //All the moves that the agent can do from the current square.
        ArrayList<Position> all_moves = getChildren();

        //The best moves that the agent can do.
        ArrayList<Position> best_moves = new ArrayList<Position>();

        //in this string we keep what the current square contains.
        String current = this.world_explored[this.current_position.getRow()][this.current_position.getColumn()];
        Position nextPosition = this.current_position;


        //if the square is OK or contains F,A or contains W or the agent already has water and the square did not contain help
        if (current.contains("OK") || (current.contains("V,A"))
                || (current.contains("F,A"))
                || (current.contains("W") && !current.contains("S") && !current.contains("H"))
                || (this.hasWater && !current.contains("H"))) {

            for (Position pos : all_moves) {
                if (!this.Visited(pos)) best_moves.add(pos);
            }

            //if there are "best" moves return one of them randomly.
            if (best_moves.size() > 0) {
                this.backTimes = 0;
                return best_moves.get(rand.nextInt(best_moves.size()));
            }

            //else do a move by going back to a visited square...
            Position temp = all_moves.get(rand.nextInt(all_moves.size()));

            //agent goes back to a visited square but not to the one that came from...
            while (temp.equals(this.previous_position)) {
                temp = all_moves.get(rand.nextInt(all_moves.size()));
            }

            this.backTimes++;
            return temp;
        }


        //if agent feels both smoke and help in current square.
        else if (current.contains("S") && current.contains("H")) {
            //we will keep all the moves that we are sure that they don't contain wumpus.
            ArrayList<Position> safe_positions = new ArrayList<Position>();

            //all the "dangerous" positions will be stored there.
            ArrayList<Position> risk_positions = new ArrayList<Position>();

            //positions that contain pit or wumpus will be stored there.
            ArrayList<Position> death_positions = new ArrayList<Position>();

            //we will keep all the moves that possibly contain villager
            ArrayList<Position> possibly_villager = new ArrayList<Position>();

            //we will keep all the moves that we are sure that they contain villager
            ArrayList<Position> villager_position = new ArrayList<Position>();


            for (Position pos : all_moves) {
                //ask if there is a fire in each one of children squares.
                int answerF = KB.Ask(new HornLiteral(LiteralSymbols.fire, true, new Constant(pos.getRow()), new Constant(pos.getColumn())), this.map.getSize());

                //ask if there is a villager in each one of children squares.
                int answerV = KB.Ask(new HornLiteral(LiteralSymbols.villager, true, new Constant(pos.getRow()), new Constant(pos.getColumn())), this.map.getSize());

                //if this square did not contains fire and villager
                if (answerF == -1 && answerV == -1) safe_positions.add(pos);

                //if this square did not contain fire and we did not sure either this square contain villager or not
                else if (answerF == -1 && answerV == 0) possibly_villager.add(pos);

                //if this square did not contain fire and contain villager
                else if (answerF == -1 && answerV == 1) villager_position.add(pos);

                //if this square did not contain villager and we did not sure either this square contain fire or not
                else if (answerF == 0 && answerV == -1) risk_positions.add(pos);

                //if we did not sure either this square contains fire and villager or not
                else if (answerF == 0 && answerV == 0) risk_positions.add(pos);

                //if this square contain villager and we did not sure either this square contain fire or not
                else if (answerF == 0 && answerV == 1) villager_position.add(pos);

                //if this square contain fire and did not contain villager
                else if (answerF == 1 && answerV == -1) death_positions.add(pos);

                //if this square contain fire and we did not sure either this square contain villager or not
                else if (answerF == 1 && answerV == 0) death_positions.add(pos);

                //if this square contain fire and villager
                else risk_positions.add(pos);
            }

            //from the above moves keep the ones that we have not visited(if any)!
            ArrayList<Position> temp_pos = new ArrayList<Position>(safe_positions);
            for (Position pos : temp_pos) {
                if (!this.Visited(pos)) {
                    best_moves.add(pos);
                    safe_positions.remove(pos);
                }
            }

            //if there is villager return that position
            if (villager_position.size() > 0) {
                return villager_position.get(0);
            }

            //if there are possibly villager return one of them randomly
            else if (possibly_villager.size() > 0) {
                return possibly_villager.get(rand.nextInt(possibly_villager.size()));
            }

            //if there are "best" moves return one of them randomly.
            else if (best_moves.size() > 0) {
                this.backTimes = 0;
                return best_moves.get(rand.nextInt(best_moves.size()));
            }

            /*
             * if the agent have not got water and have not exceeded the number of times that we can go back
             * or if we have exceeded it and the only solution is death , we go to an already visited square.
             */
            else if (!this.hasWater &&
                    ((safe_positions.size() > 0 && this.backTimes < this.backMoves)
                            || (safe_positions.size() > 0 && this.backTimes >= this.backMoves && risk_positions.isEmpty()))) {
                if (!this.hasWater) setIsRiskyToMove(1);
                this.backTimes++;
                Position temp = safe_positions.get(rand.nextInt(safe_positions.size()));

                //if there is only one safe position , go there
                if (safe_positions.size() == 1) return temp;

                //else go to the one that you did not came from...
                while (temp.equals(this.previous_position)) {
                    temp = safe_positions.get(rand.nextInt(safe_positions.size()));
                }
                return temp;
            }

            //return an undefined move.
            else if (risk_positions.size() > 0) {
                this.backTimes = 0;
                setIsRiskyToMove(0); // kenapa 0 yak.

                return risk_positions.get(rand.nextInt(risk_positions.size()));
            } else if (death_positions.size() > 0) {
                //            setLookingForHelp(-1);
                if (!this.hasWater) setIsRiskyToMove(1);
                //unavoidably, return a move that leads to the fire.
                this.backTimes = 0;
                return death_positions.get(rand.nextInt(death_positions.size()));
            } else {
                this.backTimes++;
                Position temp = safe_positions.get(rand.nextInt(safe_positions.size()));

                //if there is only one safe position , go there
                if (safe_positions.size() == 1) return temp;

                //else go to the one that you did not came from...
                while (temp.equals(this.previous_position)) {
                    temp = safe_positions.get(rand.nextInt(safe_positions.size()));
                }
                return temp;
            }


        }


        //if there is Smoke in the current square...
        else if (current.contains("S")) {
            //we will keep all the moves that we are sure that they don't contain fire.
            ArrayList<Position> safe_positions = new ArrayList<Position>();

            //all the "dangerous" positions will be stored there.
            ArrayList<Position> risk_positions = new ArrayList<Position>();

            //positions that contain fire will be stored there.
            ArrayList<Position> death_positions = new ArrayList<Position>();

            for (Position pos : all_moves) {
                //ask if there is a Fire in each one of children squares.
                int answer = KB.Ask(new HornLiteral(LiteralSymbols.fire, true, new Constant(pos.getRow()), new Constant(pos.getColumn())), this.map.getSize());

                if (answer == -1) safe_positions.add(pos);

                else if (answer == 1) death_positions.add(pos);

                else risk_positions.add(pos);
            }

            //from the above moves keep the ones that we have not visited(if any)!
            ArrayList<Position> temp_pos = new ArrayList<Position>(safe_positions);
            for (Position pos : temp_pos) {
                if (!this.Visited(pos)) {
                    best_moves.add(pos); //moves that fire free and have not visited
                    safe_positions.remove(pos); //moves that fire free and already visited
                }
            }

            //if there are "best" moves return one of them randomly.
            if (best_moves.size() > 0) {
                this.backTimes = 0;
                return best_moves.get(rand.nextInt(best_moves.size()));
            }

            /*
             * if we have not exceeded the number of times that we can go back
             * or if we have exceeded it and the only solution is death , we go to an already visited square.
             */
            else if ((safe_positions.size() > 0 && this.backTimes < this.backMoves) || (safe_positions.size() > 0 && this.backTimes >= this.backMoves && risk_positions.isEmpty()))

            {
                this.backTimes++;
                Position temp = safe_positions.get(rand.nextInt(safe_positions.size()));

                //if there is only one safe position , go there
                if (safe_positions.size() == 1) return temp;

                //else go to the one that you did not came from...
                while (temp.equals(this.previous_position)) {
                    temp = safe_positions.get(rand.nextInt(safe_positions.size()));
                }

                return temp;
            }

            //return an undefined move.
            else if (risk_positions.size() > 0) {
                this.backTimes = 0;
                return risk_positions.get(rand.nextInt(risk_positions.size()));
            }

            //unavoidably, return a move that leads to the fire.
            this.backTimes = 0;
            return death_positions.get(rand.nextInt(death_positions.size()));
        }


        //if there is Help in the current square...
        else if (current.contains("H")) {
            //we will keep all the moves that we are sure that they don't contain villager.
            ArrayList<Position> safe_positions = new ArrayList<Position>();

            //we will keep all the moves that possibly contain villager
            ArrayList<Position> possibly_villager = new ArrayList<Position>();

            //we will keep all the moves that we are sure that they contain villager
            ArrayList<Position> villager_position = new ArrayList<Position>();

            ArrayList<Position> help_position = new ArrayList<Position>();

            for (Position pos : all_moves) {
                //ask if there is a Villager in each one of children squares.
                int answer = KB.Ask(new HornLiteral(LiteralSymbols.villager, true, new Constant(pos.getRow()), new Constant(pos.getColumn())), this.map.getSize());
                boolean hasHelp = KB.knownFact(new HornLiteral(LiteralSymbols.help, true, new Constant(pos.getRow()), new Constant(pos.getColumn())));
                //if the children is villager free
                if (answer == -1) safe_positions.add(pos);

                //if the children contain villager
                else if (answer == 1) villager_position.add(pos);

                else if (hasHelp) help_position.add(pos);

                //if we did not sure either the children contain villager or not
                else possibly_villager.add(pos);
            }

            //from the above moves keep the ones that we have not visited(if any)!
            ArrayList<Position> temp_pos = new ArrayList<Position>(safe_positions);
            for (Position pos : temp_pos) {
                if (!this.Visited(pos)) {
                    best_moves.add(pos); //moves that villager free and have not visited
                    safe_positions.remove(pos); //moves that villager free and already visited
                }
            }

            //if there is villager return that position
            if (villager_position.size() > 0) {

                return villager_position.get(0);
            }

            //if there are possibly villager return one of them randomly
            else if (possibly_villager.size() > 0) {
                return possibly_villager.get(rand.nextInt(possibly_villager.size()));
            } else if (help_position.size() > 0) {
                return help_position.get(rand.nextInt(help_position.size()));
            }

            //if there are "best" moves return one of them randomly.
            else if (best_moves.size() > 0) {
                this.backTimes = 0;
                return best_moves.get(rand.nextInt(best_moves.size()));
            }

            /*
             * if the only solution is the square that we have already visited
             */
            else if (safe_positions.size() > 0) {
                Position temp = safe_positions.get(rand.nextInt(safe_positions.size()));

                //if there is only one safe position , go there
                if (safe_positions.size() == 1) return temp;

                //else go to the one that you did not came from...
                while (temp.equals(this.previous_position)) {
                    temp = safe_positions.get(rand.nextInt(safe_positions.size()));
                }

                return temp;
            }

        }


        return nextPosition;
    }


    /**
     * checks if the square given as argument is visited.
     */
    private boolean Visited(Position pos) {
        return this.world_explored[pos.getRow()][pos.getColumn()].contains("D");
    }


    //------------------------------Adding facts regarding the current world------------------------------//

    //adds the "tags" of each square.
    private void AddMapFacts() {
        this.AddInboundSquares();
        this.AddCorners();
        this.AddEdges();
        this.AddCenterSquares();
    }


    //adds to the knowledge base the squares that are in the map.
    private void AddInboundSquares() {
        for (int row = 0; row < this.world_explored.length; row++) {
            for (int col = 0; col < this.world_explored.length; col++) {

                //InBounds(row,col).
                HornLiteral fact = new HornLiteral(LiteralSymbols.inbounds, true, new Constant(row), new Constant(col));

                this.KB.addFact(fact);
            }
        }
    }


    //adds the facts for corner squares.
    private void AddCorners() {
        //Top_Left_Square(0 , 0).
        HornLiteral fact1 = new HornLiteral(LiteralSymbols.tls, true, new Constant(0), new Constant(0));

        //Top_Right_Square(0 , col-1).
        HornLiteral fact2 = new HornLiteral(LiteralSymbols.trs, true, new Constant(0), new Constant(this.world_explored.length - 1));

        //Bottom_Left_Square(row-1 , 0).
        HornLiteral fact3 = new HornLiteral(LiteralSymbols.bls, true, new Constant(this.world_explored.length - 1), new Constant(0));

        //Bottom_Right_Square(row-1 , col-1).
        HornLiteral fact4 = new HornLiteral(LiteralSymbols.brs, true, new Constant(this.world_explored.length - 1), new Constant(this.world_explored.length - 1));

        this.KB.addFact(fact1);
        this.KB.addFact(fact2);
        this.KB.addFact(fact3);
        this.KB.addFact(fact4);
    }


    //adds the facts for edge squares.
    private void AddEdges() {
        HornLiteral fact;

        //top squares(not corners).
        for (int col = 1; col < this.world_explored.length - 1; col++) {
            fact = new HornLiteral(LiteralSymbols.ts, true, new Constant(0), new Constant(col));
            this.KB.addFact(fact);
        }

        //bottom squares(not corners).
        for (int col = 1; col < this.world_explored.length - 1; col++) {
            fact = new HornLiteral(LiteralSymbols.bs, true, new Constant(this.world_explored.length - 1), new Constant(col));
            this.KB.addFact(fact);
        }

        //right squares(not corners).
        for (int row = 1; row < this.world_explored.length - 1; row++) {
            fact = new HornLiteral(LiteralSymbols.rs, true, new Constant(row), new Constant(this.world_explored.length - 1));
            this.KB.addFact(fact);
        }


        //left squares(not corners).
        for (int row = 1; row < this.world_explored.length - 1; row++) {
            fact = new HornLiteral(LiteralSymbols.ls, true, new Constant(row), new Constant(0));
            this.KB.addFact(fact);
        }
    }


    //adds the facts for center squares.
    private void AddCenterSquares() {
        for (int row = 1; row < this.world_explored.length - 1; row++) {
            for (int col = 1; col < this.world_explored.length - 1; col++) {
                HornLiteral fact = new HornLiteral(LiteralSymbols.cs, true, new Constant(row), new Constant(col));
                this.KB.addFact(fact);
            }
        }
    }


    //------------------------------Adding the rules of the game------------------------------//

    // adds the rules of the simulation to the knowledge base.
    private void AddRules() {
        this.addFireRules();
        this.addVillagerRules();
    }

    // add the rules that regard the fire
    private void addFireRules() {
        this.addFireCenterRules();
        this.addFireEdgeRules();
        this.addFireCornerRules();
    }

    // add the fire rules for the center squares.
    private void addFireCenterRules() {
        HornClause rule;
        HornLiteral lit1;
        HornLiteral lit2;
        HornLiteral lit3;
        HornLiteral lit4;
        HornLiteral lit5;
        HornLiteral lit6;
        HornLiteral lit7;


        /*
         * 	Center_Square(row,col) ^ Smoke(Add(row,1),col) ^ InBounds(Add(row,2)) ^ FireFree(Add(row,1),Sub(col,1)) ^ FireFree(Add(row,2),col) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Smoke(Add(row,1),col) ^ OutOfBounds(Add(row,2)) ^ FireFree(Add(row,1),Sub(col,1)) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Smoke(Sub(row,1),col) ^ InBounds(Sub(row,2)) ^ FireFree(Sub(row,2),col) ^ FireFree(Sub(row,1),Sub(col,1)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Smoke(Sub(row,1),col) ^ OutOfBounds(Sub(row,2)) ^ FireFree(Sub(row,1),Sub(col,1)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Smoke(row,Add(col,1)) ^ InBounds(Add(col,2)) ^ FireFree(Add(row,1),Add(col,1)) ^ FireFree(row,Add(col,2)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Smoke(row,Add(col,1)) ^ OutOfBounds(Add(col,2)) ^ FireFree(Add(row,1),Add(col,1)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         *Center_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ FireFree(Sub(row,1),Sub(col,1)) ^ FireFree(row,Sub(col,2)) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         *Center_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ OutOfBounds(Sub(col,2)) ^ FireFree(Sub(row,1),Sub(col,1)) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

    }

    // add the fire rules for the edge squares.
    private void addFireEdgeRules() {
        HornClause rule;
        HornLiteral lit1;
        HornLiteral lit2;
        HornLiteral lit3;
        HornLiteral lit4;
        HornLiteral lit5;
        HornLiteral lit6;


        /*
         * 	Left_Square(row,col) ^ Smoke(Sub(row,1),col) ^ InBounds(Sub(row,2)) ^ FireFree(Sub(row,2),col) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ Smoke(Sub(row,1),col) ^ OutOfBounds(Sub(row,2)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ Smoke(Add(row,1),col) ^ InBounds(Add(row,2)) ^ FireFree(Add(row,2),col) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ Smoke(Add(row,1),col) ^ OutOfBounds(Add(row,2)) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ Smoke(row,Add(col,1)) ^ FireFree(Sub(row,1),Add(col,1)) ^ FireFree(row,Add(col,2)) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);






        /*
         * Right_Square(row,col) ^ Smoke(Sub(row,1),col) ^ InBounds(Sub(row,2),col) ^ FireFree(Sub(row,1),Sub(col,1)) ^ FireFree(Sub(row,2),col) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Right_Square(row,col) ^ Smoke(Sub(row,1),col) ^ OutOfBounds(Sub(row,2),col) ^ FireFree(Sub(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Right_Square(row,col) ^ Smoke(Add(row,1),col) ^ InBounds(Add(row,2),col) ^ FireFree(Add(row,1),Sub(col,1)) ^ FireFree(Add(row,2),col) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Right_Square(row,col) ^ Smoke(Add(row,1),col) ^ OutOfBounds(Add(row,2),col) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Right_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ FireFree(Sub(row,1),Sub(col,1)) ^ FireFree(row,Sub(col,2)) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);






        /*
         * Top_Square(row,col) ^ Smoke(Add(row,1),col) ^ FireFree(Add(row,1),Sub(col,1)) ^ FireFree(Add(row,2),col) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Top_Square(row,col) ^ Smoke(row,Add(col,1)) ^ InBounds(Add(col,2)) ^ FireFree(Add(row,1),Add(col,1)) ^ FireFree(row,Add(col,2)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Top_Square(row,col) ^ Smoke(row,Add(col,1)) ^ OutOfBounds(Add(col,2)) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Top_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ FireFree(row,Sub(col,2)) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Top_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ OutOfBounds(Sub(col,2)) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);





        /*
         * Bottom_Square(row,col) ^ Smoke(Sub(row,1),col) ^ FireFree(Sub(row,1),Sub(col,1)) ^ FireFree(Sub(row,2),col) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Bottom_Square(row,col) ^ Smoke(row,Add(col,1)) ^ InBounds(Add(col,2)) ^ FireFree(row,Add(col,2)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Bottom_Square(row,col) ^ Smoke(row,Add(col,1)) ^ OutOfBounds(Add(col,2)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Bottom_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ FireFree(row,Sub(col,2)) ^ FireFree(Sub(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

        /*
         * Bottom_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ OutOfBounds(Sub(col,2)) ^ FireFree(Sub(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);

        this.KB.addClause(rule);

    }

    // add the fire rules for the corner squares.
    private void addFireCornerRules() {
        HornClause rule;
        HornLiteral lit1;
        HornLiteral lit2;
        HornLiteral lit3;
        HornLiteral lit4;
        HornLiteral lit5;

        /*
         * Top_Left_Square(row,col) ^ Smoke(Add(row,1),col) ^ FireFree(Add(row,2),col) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.tls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);

        /*
         * Top_Left_Square(row,col) ^ Smoke(row,Add(col,1)) ^ FireFree(row,Add(col,2)) ^ FireFree(Add(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.tls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);

        /*
         * Top_Right_Square(row,col) ^ Smoke(Add(row,1),col) ^ FireFree(Add(row,2),col) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.trs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);

        /*
         * Top_Right_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ FireFree(row,Sub(col,2)) ^ FireFree(Add(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.trs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);

        /*
         * Bottom_Left_Square(row,col) ^ Smoke(Sub(row,1),col) ^ FireFree(Sub(row,2),col) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);

        /*
         * Bottom_Left_Square(row,col) ^ Smoke(row,Add(col,1)) ^ FireFree(row,Add(col,2)) ^ FireFree(Sub(row,1),Add(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);


        /*
         * Bottom_Right_Square(row,col) ^ Smoke(Sub(row,1),col) ^ FireFree(Sub(row,2),col) ^ FireFree(Sub(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.brs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);

        /*
         * Bottom_Right_Square(row,col) ^ Smoke(row,Sub(col,1)) ^ FireFree(row,Sub(col,2)) ^ FireFree(Sub(row,1),Sub(col,1)) => Fire(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.brs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.smoke, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.firefree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.firefree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.fire, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);

        this.KB.addClause(rule);

    }

    // add the rules that regard the Villager
    private void addVillagerRules() {
        this.addVillagerCenterRules();
        this.addVillagerEdgeRules();
        this.addVillagerCornerRules();
    }

    // add the villager rules for the center squares.
    private void addVillagerCenterRules() {
        HornClause rule;
        HornLiteral lit1;
        HornLiteral lit2;
        HornLiteral lit3;
        HornLiteral lit4;
        HornLiteral lit5;
        HornLiteral lit6;
        HornLiteral lit7;
        HornLiteral lit8;
        HornLiteral lit9;
        HornLiteral lit10;
        HornLiteral lit11;

        /*
         * Center_Square(row,col) ^ Help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ VillagerFree(Add(row,1),Sub(col,1)) ^ VillagerFree(Add(row,2),col) ^ VillagerFree(Add(row,1),Add(col,1)) => Help(row,col) ^ Villager(row-1, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ Help(Add(row,2),col)
         * ^ InBounds(Add(row,3)) ^ villager free(Add(row,3),col)
         * ^ VillagerFree(Add(row,1),Sub(col,1)) ^ VillagerFree(Add(row,1),Add(col,1))
         * ^ VillagerFree(Add(row,2),Sub(col,1)) ^ VillagerFree(Add(row,2),Add(col,1)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit10 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit11 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ Help(Add(row,2),col)
         * ^ Outbounds(Add(row,2))
         * ^ VillagerFree(Add(row,1),Sub(col,1)) ^ VillagerFree(Add(row,1),Add(col,1))
         * ^ VillagerFree(Add(row,2),Sub(col,1)) ^ VillagerFree(Add(row,2),Add(col,1)) => Villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(Add(row,1),col) ^ OutOfBounds(Add(row,2)) ^ VillagerFree(Add(row,1),Sub(col,1)) ^ VillagerFree(Add(row,1),Add(col,1)) => Help(row,col) ^ Villager(row-1, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(Sub(row,1),col) ^ InBounds(Sub(row,2)) ^ VillagerFree(Sub(row,2),col) ^ VillagerFree(Sub(row,1),Sub(col,1)) ^ VillagerFree(Sub(row,1),Add(col,1)) => Help(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(Sub(row,1),col) ^ OutOfBounds(Sub(row,2)) ^ VillagerFree(Sub(row,1),Sub(col,1)) ^ VillagerFree(Sub(row,1),Add(col,1)) => Help(row,col) ^ Villager(row+1, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(sub(row,1),col) ^ InBounds(sub(row,2)) ^ Help(sub(row,2),col)
         * ^ InBounds(sub(row,3)) ^ villager free(sub(row,3),col)
         * ^ VillagerFree(sub(row,1),Sub(col,1)) ^ VillagerFree(sub(row,1),Add(col,1))
         * ^ VillagerFree(sub(row,2),Sub(col,1)) ^ VillagerFree(sub(row,2),Add(col,1)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit10 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit11 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ Help(Add(row,2),col)
         * ^ Outbounds(Add(row,2))
         * ^ VillagerFree(Add(row,1),Sub(col,1)) ^ VillagerFree(Add(row,1),Add(col,1))
         * ^ VillagerFree(Add(row,2),Sub(col,1)) ^ VillagerFree(Add(row,2),Add(col,1)) => Villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);


        /*
         * Center_Square(row,col) ^ Help(row,Add(col,1)) ^ InBounds(Add(col,2)) ^ VillagerFree(Add(row,1),Add(col,1)) ^ VillagerFree(row,Add(col,2)) ^ VillagerFree(Sub(row,1),Add(col,1)) => Help(row,col) ^ Villager(row,col-1)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(row,Add(col,1)) ^ OutOfBounds(Add(col,2)) ^ VillagerFree(Add(row,1),Add(col,1)) ^ VillagerFree(Sub(row,1),Add(col,1)) => Help(row,col) ^ Villager(row,col-1)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(row,add(col,1)) ^ InBounds(add(col,2)) ^ Help(row,add(col,2))
         * ^ InBounds(add(col,3)) ^ villager free(row,add(col,3))
         * ^ VillagerFree(sub(row,1),add(col,1)) ^ VillagerFree(add(row,1),add(col,1))
         * ^ VillagerFree(sub(row,1),add(col,2)) ^ VillagerFree(add(row,1),add(col,2)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit10 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit11 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);


        /*
         * Center_Square(row,col) ^ Help(row,sub(col-1)) ^ InBounds(sub(col,2)) ^ Help(row,sub(col,2))
         * ^ outbounds(sub(col,3))
         * ^ VillagerFree(sub(row,1),Sub(col,1)) ^ VillagerFree(add(row,1),sub(col,1))
         * ^ VillagerFree(sub(row,1),Sub(col,2)) ^ VillagerFree(add(row,1),sub(col,2)) => Villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ VillagerFree(Sub(row,1),Sub(col,1)) ^ VillagerFree(row,Sub(col,2)) ^ VillagerFree(Add(row,1),Sub(col,1)) => Help(row,col) ^ Villager(row,col+1)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(row,Sub(col,1)) ^ OutOfBounds(Sub(col,2)) ^ VillagerFree(Sub(row,1),Sub(col,1)) ^ VillagerFree(Add(row,1),Sub(col,1)) => Help(row,col) ^ Villager(row,col+1)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit8 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        rule.addLiteral(lit8);
        this.KB.addClause(rule);

        /*
         * Center_Square(row,col) ^ Help(row,sub(col-1)) ^ InBounds(sub(col,2)) ^ Help(row,sub(col,2))
         * ^ InBounds(sub(col,3)) ^ villager free(row,sub(col,3))
         * ^ VillagerFree(sub(row,1),Sub(col,1)) ^ VillagerFree(add(row,1),sub(col,1))
         * ^ VillagerFree(sub(row,1),Sub(col,2)) ^ VillagerFree(add(row,1),sub(col,2)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.cs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit10 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit11 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);


        /*
         * Center_Square(row,col) ^ Help(row,sub(col-1)) ^ InBounds(sub(col,2)) ^ Help(row,sub(col,2))
         * ^ outbounds(sub(col,3))
         * ^ VillagerFree(sub(row,1),Sub(col,1)) ^ VillagerFree(add(row,1),sub(col,1))
         * ^ VillagerFree(sub(row,1),Sub(col,2)) ^ VillagerFree(add(row,1),sub(col,2)) => Villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        rule.addLiteral(lit10);
        rule.addLiteral(lit11);
        this.KB.addClause(rule);
    }


    // add the villager rules for the edge squares.
    private void addVillagerEdgeRules() {
        HornClause rule;
        HornLiteral lit1;
        HornLiteral lit2;
        HornLiteral lit3;
        HornLiteral lit4;
        HornLiteral lit5;
        HornLiteral lit6;
        HornLiteral lit7;
        HornLiteral lit8;
        HornLiteral lit9;


        /*
         * 	Left_Square(row,col) ^ help(Sub(row,1),col) ^ InBounds(Sub(row,2)) ^ villagerFree(Sub(row,2),col) ^ villagerFree(Sub(row,1),Add(col,1)) => Help(row,col) ^ Villager(row+1, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);


        /*
         * Left_Square(row,col) ^ help(Sub(row,1),col) ^ OutOfBounds(Sub(row,2)) ^ villagerFree(Sub(row,1),Add(col,1)) => Help(row,col) ^ Villager(row+1, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * 	Left_Square(row,col) ^ help(Sub(row,1),col) ^ InBounds(Sub(row,2)) ^ help(Sub(row,2),col)
         * 	^ InBounds(Sub(row,3)) ^ villagerFree(Sub(row,3),col) ^ villagerFree(Sub(row,1),Add(col,1))
         * ^ villagerFree(Sub(row,2),Add(col,1))	=> Villager(row, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /*
         * 	Left_Square(row,col) ^ help(Sub(row,1),col) ^ InBounds(Sub(row,2)) ^ help(Sub(row,2),col)
         * 	^ outBounds(Sub(row,3)) ^ villagerFree(Sub(row,1),Add(col,1))
         * ^ villagerFree(Sub(row,1),Add(col,2))	=> Villager(row, col)
         */

        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ villagerFree(Add(row,2),col) ^ villagerFree(Add(row,1),Add(col,1)) => Help(row,col) ^ Villager(row-1, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ help(Add(row,1),col) ^ OutOfBounds(Add(row,2)) ^ villagerFree(Add(row,1),Add(col,1)) => Help(row,col) ^ Villager(row-1, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ help(Add(row,2),col)
         * ^ InBounds(Add(row,3)) ^ villagerFree(Add(row,3),col)
         * ^ villagerFree(Add(row,1),Add(col,1)) ^ villagerFree(Add(row,2),Add(col,1)) => Villager(row, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ help(Add(row,2),col)
         * ^ OutBounds(Add(row,3))
         * ^ villagerFree(Add(row,1),Add(col,1)) ^ villagerFree(Add(row,2),Add(col,1)) => Villager(row, col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /*
         * Left_Square(row,col) ^ help(row,Add(col,1)) ^ help(row,Add(col,2)) ^ villagerFree(row,Add(col,3))
         * ^ villagerFree(Sub(row,1),Add(col,1)) ^ villagerFree(Sub(row,1),Add(col,2))
         * ^ villagerFree(Add(row,1),Add(col,1)) ^ villagerFree(Add(row,1),Add(col,2))  => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);

        this.KB.addClause(rule);



        /*
         * Right_Square(row,col) ^ help(Sub(row,1),col) ^ InBounds(Sub(row,2),col) ^ villagerFree(Sub(row,1),Sub(col,1)) ^ villagerFree(Sub(row,2),col) => help(row,col) ^ villager(row+1,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);

        this.KB.addClause(rule);

        /*
         * Right_Square(row,col) ^ help(Sub(row,1),col) ^ OutOfBounds(Sub(row,2),col) ^ villagerFree(Sub(row,1),Sub(col,1)) => help(row,col) ^ villager(row+!,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Right_Square(row,col) ^ help(Sub(row,1),col) ^ InBounds(Sub(row,2),col) ^ help(Sub(row,2),col)
         * ^ InBounds(Sub(row,3),col) ^ villagerFree(Sub(row,3),col)
         * ^ villagerFree(Sub(row,1),sub(col,1)) ^ villagerFree(Sub(row,2),sub(col,1)) => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /*
         * right square(row,col) ^ help(sub(row,1),col) ^ InBounds(sub(row,2)) ^ help(sub(row,2),col)
         * ^ OutBounds(sub(row,3))
         * ^ villagerFree(sub(row,1),sub(col,1)) ^ villagerFree(sub(row,2),sub(col,1)) => Villager(row, col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);


        /*
         * Right_Square(row,col) ^ help(Add(row,1),col) ^ InBounds(Add(row,2),col) ^ villagerFree(Add(row,1),Sub(col,1)) ^ villagerFree(Add(row,2),col) => help(row,col) ^ villager(row-1,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Right_Square(row,col) ^ help(Add(row,1),col) ^ OutOfBounds(Add(row,2),col) ^ villagerFree(Add(row,1),Sub(col,1)) => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * right square(row,col) ^ help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ help(Add(row,2),col)
         * ^ InBounds(Add(row,3)) ^ villagerFree(Add(row,3),col)
         * ^ villagerFree(Add(row,1),sub(col,1)) ^ villagerFree(Add(row,2),sub(col,1)) => Villager(row, col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /*
         * right square(row,col) ^ help(Add(row,1),col) ^ InBounds(Add(row,2)) ^ help(Add(row,2),col)
         * ^ OutBounds(Add(row,3))
         * ^ villagerFree(Add(row,1),sub(col,1)) ^ villagerFree(Add(row,2),sub(col,1)) => Villager(row, col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /*
         * right_Square(row,col) ^ help(row,sub(col,1)) ^ help(row,sub(col,2)) ^ villagerFree(row,sub(col,3))
         * ^ villagerFree(Sub(row,1),sub(col,1)) ^ villagerFree(Sub(row,1),sub(col,2))
         * ^ villagerFree(Add(row,1),sub(col,1)) ^ villagerFree(Add(row,1),sub(col,2))  => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.rs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);






        /* v
         * top square(row,col) ^ help(row+1,col) ^ help(row+2,col) ^ villagerFree(row+3,col)
         * ^ villagerFree(add(row,1),add(col,1)) ^ villagerFree(add(row,2),add(col,1))
         * ^ villagerFree(Add(row,1),sub(col,1)) ^ villagerFree(add(row,1),sub(col,1))  => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* >
         * Top_Square(row,col) ^ help(row,Add(col,1)) ^ InBounds(Add(col,2)) ^ villagerFree(Add(row,1),Add(col,1)) ^ villagerFree(row,Add(col,2)) => help(row,col) ^ villager(row, col-1)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /* >
         * Top_Square(row,col) ^ help(row,Add(col,1)) ^ OutOfBounds(Add(col,2)) ^ villagerFree(Add(row,1),Add(col,1)) => help(row,col) ^ villager(row, col-1)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /* >
         * Top_Square(row,col) ^ help(row,add(col,1)) ^ InBounds(add(col,2)) ^ help(row,add(col,2))
         * ^ InBounds(add(col,3)) ^ villagerFree(row,add(col,3))
         * ^ villagerFree(Add(row,1),add(col,1)) ^ villagerFree(Add(row,1),add(col,2)) => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* >
         * Top_Square(row,col) ^ help(row,add(col,1)) ^ InBounds(add(col,2)) ^ help(row,add(col,2))
         * ^ OutBounds(add(col,3))
         * ^ villagerFree(Add(row,1),add(col,1)) ^ villagerFree(Add(row,1),add(col,2)) => villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* <
         * Top_Square(row,col) ^ help(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ help(row,Sub(col,2))
         * ^ InBounds(Sub(col,3)) ^ villagerFree(row,Sub(col,3))
         * ^ villagerFree(Add(row,1),Sub(col,1)) ^ villagerFree(Add(row,1),Sub(col,2)) => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* <
         * Top_Square(row,col) ^ help(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ help(row,Sub(col,2))
         * ^ OutBounds(Sub(col,3))
         * ^ villagerFree(Add(row,1),Sub(col,1)) ^ villagerFree(Add(row,1),Sub(col,2)) => villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* <
         * Top_Square(row,col) ^ help(row,sub(col,1)) ^ InBounds(sub(col,2)) ^ villagerFree(Add(row,1),sub(col,1)) ^ villagerFree(row,sub(col,2)) => help(row,col) ^ villager(row, col+1)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /* <
         * Top_Square(row,col) ^ help(row,sub(col,1)) ^ OutOfBounds(sub(col,2)) ^ villagerFree(Add(row,1),sub(col,1)) => help(row,col) ^ villager(row, col+1)
         */
        lit1 = new HornLiteral(LiteralSymbols.ts, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);






        /* v
         * bottom(row,col) ^ help(row-1,col) ^ help(row-2,col) ^ villagerFree(row-3,col)
         * ^ villagerFree(sub(row,1),add(col,1)) ^ villagerFree(sub(row,2),add(col,1))
         * ^ villagerFree(sub(row,1),sub(col,1)) ^ villagerFree(sub(row,1),sub(col,1))  => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* >
         * bottom(row,col) ^ help(row,Add(col,1)) ^ InBounds(Add(col,2)) ^ villagerFree(sub(row,1),Add(col,1)) ^ villagerFree(row,Add(col,2)) => help(row,col) ^ villager(row, col-1)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /* >
         * bottom_Square(row,col) ^ help(row,Add(col,1)) ^ OutOfBounds(Add(col,2)) ^ villagerFree(sub(row,1),Add(col,1)) => help(row,col) ^ villager(row, col-1)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /* >
         * bottom(row,col) ^ help(row,add(col,1)) ^ InBounds(add(col,2)) ^ help(row,add(col,2))
         * ^ InBounds(add(col,3)) ^ villagerFree(row,add(col,3))
         * ^ villagerFree(sub(row,1),add(col,1)) ^ villagerFree(sub(row,1),add(col,2)) => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* >
         * bottom(row,col) ^ help(row,add(col,1)) ^ InBounds(add(col,2)) ^ help(row,add(col,2))
         * ^ OutBounds(add(col,3))
         * ^ villagerFree(sub(row,1),add(col,1)) ^ villagerFree(sub(row,1),add(col,2)) => villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* <
         * bottom(row,col) ^ help(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ help(row,Sub(col,2))
         * ^ InBounds(Sub(col,3)) ^ villagerFree(row,Sub(col,3))
         * ^ villagerFree(sub(row,1),Sub(col,1)) ^ villagerFree(sub(row,1),Sub(col,2)) => villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit5 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit7 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit8 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit9 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* <
         * bottom(row,col) ^ help(row,Sub(col,1)) ^ InBounds(Sub(col,2)) ^ help(row,Sub(col,2))
         * ^ OutBounds(Sub(col,3))
         * ^ villagerFree(sub(row,1),Sub(col,1)) ^ villagerFree(sub(row,1),Sub(col,2)) => villager(row,col)
         */
        lit5 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        rule.addLiteral(lit8);
        rule.addLiteral(lit9);
        this.KB.addClause(rule);

        /* <
         * bottom(row,col) ^ help(row,sub(col,1)) ^ InBounds(sub(col,2)) ^ villagerFree(sub(row,1),sub(col,1)) ^ villagerFree(row,sub(col,2)) => help(row,col) ^ villager(row, col+1)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.inbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /* <
         * bottom(row,col) ^ help(row,sub(col,1)) ^ OutOfBounds(sub(col,2)) ^ villagerFree(sub(row,1),sub(col,1)) => help(row,col) ^ villager(row, col+1)
         */
        lit1 = new HornLiteral(LiteralSymbols.bs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.outbounds, false, new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.help, true, new Variable("row"), new Variable("col"));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit6);
        this.KB.addClause(rule);

        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);
    }

    // add the villager rules for the corner squares.
    private void addVillagerCornerRules() {
        HornClause rule;
        HornLiteral lit1;
        HornLiteral lit2;
        HornLiteral lit3;
        HornLiteral lit4;
        HornLiteral lit5;
        HornLiteral lit6;
        HornLiteral lit7;

        /*
         * Top_Left_Square(row,col) ^ Help(Add(row,1),col) ^ Help(Add(row,2),col) ^ VillagerFree(Add(row,1),Add(col,1)) ^ VillagerFree(Add(row,2),Add(col,1)) ^ VillagerFree(Add(row,3),col) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.tls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Top_Left_Square(row,col) ^ Help(row,Add(col,1)) ^ Help(row,Add(col,2)) ^ VillagerFree(row,Add(col,3)) ^ VillagerFree(Add(row,1),Add(col,1)) ^ VillagerFree(Add(row,1),Add(col,2)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.tls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Top_Right_Square(row,col) ^ Help(Add(row,1),col) ^ Help(Add(row,2),col) ^ VillagerFree(Add(row,1),Sub(col,1)) ^ VillagerFree(Add(row,2),Sub(col,1)) ^ VillagerFree(Add(row,3),col) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.trs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(3)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Top_Right_Square(row,col) ^ Help(row,Sub(col,1)) ^ Help(row,Sub(col,2)) ^ VillagerFree(row,Sub(col,3)) ^ VillagerFree(Add(row,1),Sub(col,1)) ^ VillagerFree(Add(row,1),Sub(col,2)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.trs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.add, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Bottom_left_Square(row,col) ^ Help(sub(row,1),col) ^ Help(sub(row,2),col) ^ VillagerFree(sub(row,1),add(col,1)) ^ VillagerFree(sub(row,2),add(col,1)) ^ VillagerFree(aub(row,3),col) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Bottom_left_square(row,col) ^ Help(row,add(col,1)) ^ Help(row,add(col,2)) ^ VillagerFree(row,add(col,3)) ^ VillagerFree(sub(row,1),add(col,1)) ^ VillagerFree(sub(row,1),add(col,2)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.bls, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.add, new Variable("col"), new Constant(3)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.add, new Variable("col"), new Constant(2)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);


        /*
         * Bottom_right_Square(row,col) ^ Help(sub(row,1),col) ^ Help(sub(row,2),col) ^ VillagerFree(sub(row,1),sub(col,1)) ^ VillagerFree(sub(row,2),sub(col,1)) ^ VillagerFree(aub(row,3),col) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.brs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Variable("col"));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Variable("col"));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(3)), new Variable("col"));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(2)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);

        /*
         * Bottom_right_square(row,col) ^ Help(row,sub(col,1)) ^ Help(row,sub(col,2)) ^ VillagerFree(row,sub(col,3)) ^ VillagerFree(sub(row,1),sub(col,1)) ^ VillagerFree(sub(row,1),sub(col,2)) => Villager(row,col)
         */
        lit1 = new HornLiteral(LiteralSymbols.brs, false, new Variable("row"), new Variable("col"));
        lit2 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit3 = new HornLiteral(LiteralSymbols.help, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit4 = new HornLiteral(LiteralSymbols.villagerfree, false, new Variable("row"), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(3)));
        lit5 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(1)));
        lit6 = new HornLiteral(LiteralSymbols.villagerfree, false, new Function(FunctionSymbols.sub, new Variable("row"), new Constant(1)), new Function(FunctionSymbols.sub, new Variable("col"), new Constant(2)));
        lit7 = new HornLiteral(LiteralSymbols.villager, true, new Variable("row"), new Variable("col"));


        rule = new HornClause();
        rule.addLiteral(lit1);
        rule.addLiteral(lit2);
        rule.addLiteral(lit3);
        rule.addLiteral(lit4);
        rule.addLiteral(lit5);
        rule.addLiteral(lit6);
        rule.addLiteral(lit7);
        this.KB.addClause(rule);
    }

}
