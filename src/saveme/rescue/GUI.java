package saveme.rescue;
/*
 * Kelas ini berfungsi untuk membaca output file dari program dan merepresentasikannya
 * di GUI.
 *
 * @author Anisha Inas Izdihar & Aviliani Pramestya
 * @version March 30 2017
 */

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

public class GUI {

    private GridBagConstraints c;
    private static int PIXELS_PER_INCH;
    private JLabel status;
    private JLabel villager;
    private JLabel energy;
    private JLabel water;
    private JPanel mainPanel;
    private boolean hasFoundWater;
    private JLabel[][] map;


    public GUI() {
        this.map = new JLabel[6][6];
        this.hasFoundWater = false;
        start();

    }

    // --------------------------------------------- SETTER GETTER ----------------------------------------------------
    private JLabel getStatus() {
        return status;
    }

    private void setStatus(String status) {
        this.status.setText(status);
    }

    private int getVillager() {
        return Integer.parseInt(this.villager.getText().substring(this.villager.getText().length() - 1));
    }

    private void setVillager(int villager) {
        this.villager.setText("Villager saved: " + villager);
    }

    private int getEnergy() {
        return Integer.parseInt(this.energy.getText().substring(8));
    }

    private void setEnergy(int energy) {
        this.energy.setText("Energy: " + energy);
    }

    /**
     * Method ini membuat home page yang berisi judul program button untuk lanjut ke main page
     */
    private void start() {
        Toolkit tk = Toolkit.getDefaultToolkit();
        PIXELS_PER_INCH = tk.getScreenResolution();

        // Create main frame
        JFrame mainFrame = new JFrame("SaveMe!");
        mainFrame.setSize(7 * PIXELS_PER_INCH + 95, 7 * PIXELS_PER_INCH);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setResizable(true);


        // Create Main Panel
        mainPanel = new JPanel(new GridBagLayout());
        c = new GridBagConstraints();
        mainPanel.setBackground(new Color(236, 239, 248));

        // Add label on homepage
        JLabel label = new JLabel("THE VILLAGE IS ON FIRE!");
        label.setForeground(new Color(0x3e6172));
        Font fontWelcome = new Font("Century Gothic", Font.BOLD, 30);
        label.setFont(fontWelcome);
        c.gridx = 0;
        c.gridy = 0;
        mainPanel.add(label, c);

        // Add button to homepage
        JButton saveMe = makeButton("SaveMe!");
        saveMe.setBackground(new Color(0xf16900));
        saveMe.setForeground(new Color(236, 239, 248));
        saveMe.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(new Color(236, 239, 248), 5),
                BorderFactory.createEmptyBorder(5, 10, 10, 10)));

        c.gridx = 0;
        c.gridy = 1;
        mainPanel.add(saveMe, c);

        mainFrame.add(mainPanel);
        mainPanel.setVisible(true);
        mainFrame.setVisible(true);

        // Create action listener for button
        saveMe.addActionListener(e -> {
            mainPanel.removeAll();
            mainPanel.revalidate();
            mainPanel.repaint();

            startJourney();
        });
    }

    /**
     * Method ini membuat peta dari village dan menampilkan perjalanan agent
     */
    private void startJourney() {

        // create main page
        mainPanel.setOpaque(true);
        mainPanel.setBackground(new Color(0xefeef4));

        // Create save me label
        JLabel saveMeLabel = new JLabel("SaveMe!");
        saveMeLabel.setForeground(new Color(0x3e6172));
        Font fontSaveMe = new Font("Century Gothic", Font.BOLD, 40);
        saveMeLabel.setFont(fontSaveMe);
        saveMeLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        c.gridx = 0;
        c.gridy = 0;
        mainPanel.add(saveMeLabel, c);

        Font fontStatus = new Font("Century Gothic", Font.PLAIN, 30);

        // Create water label
        this.water = new JLabel("Agent hasn't acquired water");
        water.setForeground(new Color(0x3e6172));
        water.setFont(fontStatus);
        water.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 1;
        mainPanel.add(water, c);

        // Create energy label
        this.energy = new JLabel("Energy: 100");
        energy.setForeground(new Color(0x3e6172));
        energy.setFont(fontStatus);
        energy.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        c.gridx = 0;
        c.gridy = 2;
        mainPanel.add(energy, c);

        // Create villager label
        this.villager = new JLabel("Villager saved: 0");
        villager.setForeground(new Color(0x3e6172));
        villager.setFont(fontStatus);
        villager.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        c.gridx = 0;
        c.gridy = 3;
        mainPanel.add(villager, c);

        // Create status label
        this.status = new JLabel("");
        status.setForeground(new Color(0x3e6172));
        status.setFont(fontStatus);
        status.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        c.gridx = 0;
        c.gridy = 5;
        mainPanel.add(status, c);

        // Add gap between panel
        JPanel filler = new JPanel();
        filler.setPreferredSize(new Dimension(200, 20));
        filler.setOpaque(true);
        filler.setBackground(new Color(0xefeef4));
        filler.setBorder(BorderFactory.createEmptyBorder(20, 10, 10, 10));

        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 2;
        mainPanel.add(filler, c);
        createMap();

        // creating timer task, timer
        TimerTask printGUI = new printGUI();
        Timer timer = new Timer();

        // scheduling the task at fixed rate
        timer.scheduleAtFixedRate(printGUI, new Date(), 700);
    }

    private void createMap() {

        // CREATE MAP PANEL
        GridLayout mapLayout = new GridLayout(6, 6);
        JPanel mapPanel = new JPanel(mapLayout);

        mapPanel.setPreferredSize(new Dimension(9 * PIXELS_PER_INCH, 6 * PIXELS_PER_INCH));
        mapPanel.setBackground(new Color(0xefeef4));
        mapPanel.setForeground(new Color(0xefeef4));
        mapPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        for (int i = 0; i < 6; i++) {

            c.gridx = i;

            for (int j = 0; j < 6; j++) {

                c.gridy = j;

                ImageIcon myPicture = getImage("-");
                JLabel image = new JLabel(myPicture);

                image.setBorder(BorderFactory.createCompoundBorder(
                        BorderFactory.createLineBorder(new Color(0xFF8729), 1),
                        BorderFactory.createEmptyBorder(1, 1, 1, 1)));
                image.setOpaque(true);
                image.setSize(30, 30);

                this.map[i][j] = image;

                mapPanel.add(image, c);
            }
        }

        c.gridx = 2;
        c.gridy = 0;
        c.gridheight = 6;
        mainPanel.add(mapPanel, c);
    }


    /**
     * Method ini berfungsi untuk membuat button
     * button akan diberi warna serta action listener
     *
     * @param text Text yang tertera pada button
     * @return JButton mengembalikan button yang telah disetting
     */
    private JButton makeButton(String text) {
        // Add button
        JButton button = new JButton(text);
        button.setContentAreaFilled(true);
        button.setBackground(new Color(0xb2d4dc));
        button.setForeground(new Color(0xfafaff));
        Font font = new Font("Century Gothic", Font.BOLD, 25);
        button.setFont(font);
        button.setFocusPainted(false);

        button.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(new Color(0xfafaff), 3),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        button.getModel().addChangeListener(new ChangeListener() {


            @Override
            public void stateChanged(ChangeEvent e) {
                ButtonModel model = (ButtonModel) e.getSource();

                if (model.isRollover()) {
                    button.setBackground(new Color(0xfedc18));
                } else {
                    button.setBackground(new Color(0xf16900));
                }
            }
        });

        return button;
    }

    /**
     * Method ini berfungsi untuk mendapatkan gambar yang sesuai dengan
     * input yang telah diberikan
     *
     * @param type Tipe dari gambar yang diinginkan
     * @return ImageIcon gambar yang sesuai dengan input
     */
    private ImageIcon getImage(String type) {

        BufferedImage myPicture = null;
        try {

            String image = "";
            String status = "";

            if (type.contains("A")) {

                if (type.contains("F")) {

                    image = "res/AF.PNG";
                    status = "You stepped into fire!";

                    if (this.hasFoundWater) {
                        status = "You put out the fire.";
                    } else {
                        setEnergy(getEnergy() - 10);
                    }

                } else if (type.contains("V")) {

                    image = "res/AV.PNG";
                    status = "You found a villager!";
                    setVillager(getVillager() + 1);

                } else if (type.contains("W")) {

                    image = "res/AW.PNG";
                    status = "You found water!";
                    this.hasFoundWater = true;
                    this.water.setText("Agent has acquired water");

                } else if (type.contains("S") && type.contains("H")) {
                    image = "res/ASH.PNG";
                } else if (type.contains("S")) {
                    image = "res/AS.PNG";
                } else if (type.contains("H")) {
                    image = "res/AH.PNG";
                } else {
                    image = "res/A.PNG";
                }

                this.status.setText(status);

            } else if (type.contains("F")) {
                image = "res/F.PNG";
            } else if (type.contains("V")) {
                image = "res/V.PNG";
            } else if (type.contains("S") && type.contains("H")) {
                image = "res/SH.PNG";
            } else if (type.contains("S")) {
                image = "res/S.PNG";
            } else if (type.contains("H")) {
                image = "res/H.PNG";
            } else if (type.contains("W")) {
                image = "res/W.PNG";
            } else if (type.contains("OK")) {
                image = "res/OK.PNG";
            } else {
                image = "res/-.PNG";
            }

            myPicture = ImageIO.read(new File(image));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ImageIcon(myPicture);

    }

    /**
     * Kelas ini adalah kelas yang mengextend TimerTask untuk menjalankan tugas yaitu
     * mencetak perubahan langkah agent di villager setiap 0.7 detik
     */
    private class printGUI extends TimerTask {

        private StringTokenizer moves;

        public printGUI() {
            this.moves = getMoves();
        }

        /**
         * Method ini berfungsi untuk membaca file out.txt yang berisi kondisi village
         * selama program berjalan
         *
         * @return StringTokenizer string panjang yang merupakan kumpulan
         * kondisi village dalam satu turn yang dipisahkan dengan string 'xxx'
         */
        private StringTokenizer getMoves() {

            File file = new File("out.txt");
            FileInputStream fis;
            String text = "";

            try {
                fis = new FileInputStream(file);
                byte[] data = new byte[(int) file.length()];
                fis.read(data);
                text = new String(data, "UTF-8");
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new StringTokenizer(text, "xxx");
        }

        /**
         * Method ini berfungsi untuk menampilkan gambar setiap kotak di map
         * dalam suatu waktu
         *
         * @param moves String yang berisi kondisi village dalam satu turn
         */
        private void print(String moves) {

            StringTokenizer tokenizer = new StringTokenizer(moves, "\n");

            for (int row = 0; row < 6; row++) {
                for (int col = 0; col < 6; col++) {

                    String s = tokenizer.nextToken().trim();
                    ImageIcon picture = getImage(s);
                    map[row][col].setIcon(picture);

                }
            }
        }

        /**
         * Setiap 0.7 detik GUI akan mengganti gambar pada map dengan konisi village yang bersesuaian
         * GUI akan terus mencetak map hingga game telah berakhir karena agent kehabisan energy atau sudah
         * menyelamatkan seluruh villager
         */
        public void run() {

            if (moves.hasMoreTokens()) {
                String text = moves.nextToken();
                print(text);
            } else {
                gameOver();
            }

        }

        /**
         * Method ini berfungsi untuk menampilkan pesan sesuai dengan alasan berhentinya game
         */
        private void gameOver() {
            if (getVillager() < 2) {
                setEnergy(0);
                setStatus("GAME OVER");
            } else {
                setVillager(2);
                setStatus("YOU SAVED ALL VILLAGERS!");
            }

        }


    }
}